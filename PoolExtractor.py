#! /usr/bin/python

import cv2
import sys
import numpy as np

def ScaleRect(rect, scale):
    return [
        round(rect[0] - rect[2] * (scale - 1) / 2),
        round(rect[1] - rect[3] * (scale - 1) / 2),
        round(rect[2] * scale),
        round(rect[3] * scale)
    ]

def extract(img, contour):

    big = True

    # I don't know why this shape is (y, x) and not (x, y)
    img_size = (
        img.shape[:2][1],
        img.shape[:2][0]
    )

    big_img_size = (
        img.shape[:2][1] * 2,
        img.shape[:2][0] * 2
    )


    # Make the image twice bigger, filled with black pixels
    # Just in case the pool would be on the edge, to still be able to get 1.5 the bounding box
    big_img = np.zeros((big_img_size[1], big_img_size[0], 3), np.uint8)
    # Copy img into big_img
    big_img[round(img_size[1] * 0.5):round(img_size[1] * 1.5), round(img_size[0] * 0.5):round(img_size[0] * 1.5)] = img


    # Find minimum rotated rectangle
    # r is formed like ( center (x,y), (width, height), angle of rotation )
    r = cv2.minAreaRect(contour)

    # Move rectangle center to match new image
    center = (r[0][0] + img_size[0] * 0.5, r[0][1] + img_size[1] * 0.5)
    size = r[1]

    # We arrange angle to make sure to have a "horizontal" pool
    angle = r[2]
    if size[0] < size[1]:
        angle += 90.0
        # swap
        size = (size[1], size[0])

    # Calculate rotation matrix
    rotation_matrix = cv2.getRotationMatrix2D(center, angle, 1.0)

    # Apply rotation
    img2 = cv2.warpAffine(big_img, rotation_matrix, big_img_size, cv2.INTER_CUBIC);

    new_r = [
        center[0] - size[0] / 2,
        center[1] - size[1] / 2,
        size[0],
        size[1]
    ]
    r_scale = ScaleRect(new_r, 1.5)

    # formula is img[y1:y2, x1:x2]
    img = img2[r_scale[1]:r_scale[1] + r_scale[3], r_scale[0]:r_scale[0] + r_scale[2]]

    return img

#! /usr/bin/python
import sys, errno
import joblib
import cv2
import PoolExtractor
import PoolDetector
import argparse
import my_utils

def proccessImage(img, clf, _id):
    # Detect contours
    contours = PoolDetector.processImage(img)

    found = []
    j = 0
    for contour in contours:
        # Extract the cleaned possible pool
        pool = PoolExtractor.extract(img, contour)

        # Run classifier on possible pool
        X_test = [ my_utils.getDataFeatures(pool) ]
        y_pred = clf.predict(X_test)

        if y_pred[0] == 1:
            found.append(contour)

        j += 1

    for contour in found:
        # Calculate contour moment
        M = cv2.moments(contour)
        # Calcuate contour center
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])

        # cross the pool
        cv2.line(img, (cX - 4, cY), (cX + 4, cY), (0, 0, 255), 1)
        cv2.line(img, (cX, cY - 4), (cX, cY + 4), (0, 0, 255), 1)

        #res = cv2.drawContours(img, contours, -1, (0, 255, 0), 1)

    cv2.imwrite('dl_data/' + str(len(found)) + "-"  + str(len(contours)) + f"_{_id}.png", img)

    return (img, contours, found)


parser = argparse.ArgumentParser(description="Run the classifier on te given file. One of the two parameters have to be used")
parser.add_argument('--cadastre', '-c', help="Run the classifier on the whole cadastre file")
parser.add_argument('--image', '-i', help="Run the classifier only on the image")
args = parser.parse_args()


if (not (args.image or args.cadastre)):
    print("One of the two parameter has to be given. Use help for more info")
    sys.exit(errno.EINVAL)

clf = joblib.load('clf.joblib')
my_utils.cleanDLData()

if (args.image):
    img = cv2.imread(args.image)
    (img, contours, found) = proccessImage(img, clf, "result")
    print(str(len(found)) + " pool(s) found")
    sys.exit(0)

# ELSE --cadastre
features = my_utils.loadCadastre(args.cadastre)
_len = len(features)

i = 1
for feature in features:
    file_path = 'cache/feature_' + feature['id'] + '.png'

    _id = 'feature_' + feature['id']
    img = cv2.imread(file_path)

    (img, contours, found) = proccessImage(img, clf, _id)

    print("+" if len(found) > 9 else len(found), end="", flush=True)

    if i % 100 == 0:
        print(f"  {i}/{_len}")

    i += 1

print(f"  {i - 1}/{_len}\nDone.")

#! /usr/bin/python
import sys

from ImageLoader import *
import os
import my_utils

features = my_utils.loadCadastre(sys.argv[1])
_len = len(features)

loader = ImageLoader()
i = 1

for feature in features:
    cache_path = 'cache/feature_' + feature['id'] + '.png'

    if not os.path.isfile(cache_path):
        print("X", end="", flush=True)
        img, corner = loader.geoJSON_to_satellite_view(feature)
        cv2.imwrite(cache_path, img)
    else:
        print("O", end="", flush=True)


    if i % 100 == 0:
        print(f"  {i}/{_len}")

    i += 1

print(f"  {i - 1}/{_len}\nDone.")

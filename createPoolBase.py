#! /usr/bin/python

import sys
import cv2
import my_utils
import PoolDetector
import PoolExtractor

my_utils.cleanDLData()

features = my_utils.loadCadastre(sys.argv[1])
_len = len(features)

i = 1
for feature in features:
    _id = 'feature_' + feature['id']
    file_path = f'cache/{_id}.png'

    img = cv2.imread(file_path)
    contours = PoolDetector.processImage(img)

    j = 0
    for contour in contours:
        pool = PoolExtractor.extract(img, contour)
        cv2.imwrite(f"data/{_id}_{j}.png", pool)
        j += 1

    res = cv2.drawContours(img, contours, -1, (0, 0, 255), 2)
    cv2.imwrite('dl_data/' + str(len(contours)) + f"_{_id}.png", res)

    print("+" if len(contours) > 9 else len(contours), end="", flush=True)

    if i % 100 == 0:
        print(f"  {i}/{_len}")

    i += 1

print(f"  {i - 1}/{_len}\nDone.")

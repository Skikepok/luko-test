import os
import cv2
import json

def cleanDLData():
    print('Cleaning "dl_data" folder...', end="", flush=True)
    for filename in os.listdir("dl_data"):
        # clean only png files
        if (not ".png" in filename):
            continue

        file_path = os.path.join("dl_data", filename)
        try:
            os.unlink(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))
    print("done.")


def getDataFeatures(img):
    img_res = cv2.resize(img, (15, 15))
    blur = cv2.GaussianBlur(img_res, (3, 3), 0)

    return blur.ravel().tolist()

def loadCadastre(path):
    print(f"Loading file {path}...", end="")
    data = json.load(open(path))
    features = data["features"]
    _len = len(features)
    print(f" => {_len} features found")
    return features

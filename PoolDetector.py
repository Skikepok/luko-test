
import numpy as np
import cv2

def processImage(image, debug = False):
    # Blur it a little to spread the blue around
    blur = cv2.GaussianBlur(image, (3, 3), 0)
    if debug:
        cv2.imwrite('debug_0_blurred.png', blur)

    # Switch to HSV color
    image_HSV = cv2.cvtColor(blur, cv2.COLOR_BGR2HSV)

    # Select only pixels that are blue-ish and not too dark or too grey
    mask = cv2.inRange(image_HSV, (85, 20 / 100 * 255, 40 / 100 * 255), (120, 255, 255))
    output = cv2.bitwise_and(image, image, mask = mask)

    if debug:
        cv2.imwrite('debug_1_initial.png', image)
        cv2.imwrite('debug_2_inrange.png', output)

    # Erode the image to remove pixels noise
    erosion_rect = cv2.erode(mask, cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3)), iterations = 1)
    # And then dilate it to come back to something slightly bigger than before
    dilation_rect = cv2.dilate(erosion_rect, cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3)), iterations = 2)

    if debug:
        cv2.imwrite('debug_3_erode.png', cv2.bitwise_and(image, image, mask = erosion_rect))
        cv2.imwrite('debug_4_dilate.png', cv2.bitwise_and(image, image, mask = dilation_rect))

    # Find pools contours and draw them on the initial image
    contours, hierarchy = cv2.findContours(dilation_rect, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    return contours

#! /usr/bin/python

import cv2
import PoolDetector

image = cv2.imread("subject.png")

contours = PoolDetector.processImage(image, True)

res = cv2.drawContours(image, contours, -1, (0, 0, 255), 2)

cv2.imwrite('debug_final.png', res)

#! /usr/bin/python

import numpy as np
import cv2
import os.path
from os import walk
import csv
import my_utils

data = []

for (root, dirs, files) in os.walk('data/'):
    for file in files:
        if not ".png" in file:
            continue

        img = cv2.imread(root + "/" + file)
        d = my_utils.getDataFeatures(img)

        d.append(1 if root == "data/yes" else 0)
        data.append(d)

print(str(len(data)) + " data in the dataset")

with open('data/dataset.csv', 'w') as f:
    writer = csv.writer(f)
    writer.writerows(data)

all:

clean:
	-rm -rf dl_data/*
	-rm -rf data/*.png
	-rm -rf __pycache__/

create-cache:
	@echo "# Sometime it segfaults..."
	@echo "# Error redirection is because of some warning from PyProj lib, very annoying"
	for i in cadastre/*; do ./loadCache.py $$i 2>/dev/null ; done

test-cache:
	tar xvf test-cache.tar.bz2

extract-pools:
	for i in cadastre/*; do ./createPoolBase.py $$i ; done

dataset:
	./createDataset.py

classifier: clf.joblib

clf.joblib: data/dataset.csv
	./SVMCreator.py

.PHONY: all clean create-cache extract-pool dataset test-cache

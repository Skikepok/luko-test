# Pool Identifier

## Introduction

This project has several steps to see it's full potential. If you want to skip some steps, the git is provided with data and a functional classifier. You can try directly the ./run.py file. I didn't include any cache but I let the file `cadastre/cadastre-68034-parcelles.json` for testing purposes.

## Create cadastre cache

To do so, fill the folder `cadastre/` with the json files downloaded from [GeoJson](https://cadastre.data.gouv.fr/data/etalab-cadastre/2019-10-01/geojson/communes/) and run the command:

```bash
$ make create-cache
```

It will download all the images of all the cadastres and store them in the `cache/` folder

You can also run the following command to load only one cadastre file in cache:

```bash
$ ./loadCache.py cadastre_file
```

For testing purposes, you can download the cache of the `cadastre/cadastre-68034-parcelles.json` file with the following command: (downloading the normal cache can be very long)

```bash
$ make test-cache
```

## Create your dataset

Once the cache is filled, you can create your swiming pool set of data. To do so run the command:

```bash
$ make extract-pools
```

It will analyse all the cache files and extract "possible pools", straighten them and store them in the `data/` folder. Now all you have to do is simply separate real pools from false positives __by hand__ and store them in the `data/yes/` and `data/no/` folders. Some pools are already placed there so you don't necessarily need to do it yourself.

You can also run the following command to analyse only one cadastre file:

```bash
$ ./createPoolBase.py cadastre_file
```

Once it's done, run the command:

```bash
$ make dataset
```

It will combine all the labelized pools from the `data/yes/` and `data/no/` folders into a csv file containing the dataset.

## Create the Classifier

Now that the dataset is created, all we need to do is create the classifier with the command:

```bash
$ make classifier
```

It will create the classifier from the dataset, test it and print the result

## Use your newly created classifier

To use your classifier, run the command:

```bash
$ ./run.py [--cadastre/-c json_file | --image/-i image]
```

The results will be stored in the dl_data folder and the files that start with non zero will have pools detected and marked on the image.

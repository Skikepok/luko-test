
from threading import Thread
from urllib.request import urlopen
import numpy as np
import cv2

class ThreadImager(Thread):
    def __init__(self, col, row):
        Thread.__init__(self)
        self.col = col
        self.row = row
        self.image = None

    def run(self):
        self.image = self.url_to_image()
        return True

    def get_tiles_url(self):
        return f"http://wxs.ign.fr/pratique/geoportail/wmts?service=WMTS&request=GetTile" \
              f"&version=1.0.0&layer=ORTHOIMAGERY.ORTHOPHOTOS" \
              f"&tilematrixset=PM&tilematrix=19&" \
              f"tilecol={self.col}&" \
              f"tilerow={self.row}&" \
              f"layer=ORTHOIMAGERY.ORTHOPHOTOS&format=image/jpeg&style=normal"


    def url_to_image(self, readFlag=cv2.IMREAD_COLOR):
        resp = urlopen(self.get_tiles_url())
        image = np.asarray(bytearray(resp.read()), dtype="uint8")
        image = cv2.imdecode(image, readFlag)
        return image

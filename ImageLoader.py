import json
import cv2
import numpy as np

from ThreadImager import *
from pyproj import transform, Proj
from shapely.geometry import shape
from shapely.ops import unary_union

class ImageLoader:
    def geoJSON_to_satellite_view(self, feature):
        parcelle = shape(feature['geometry'])
        minx, miny, maxx, maxy = parcelle.bounds

        min_col, max_col, min_row, max_row = self.get_titles_list(minx, miny, maxx, maxy)

        img = None
        thread_img = {}

        for row in range(min_row, max_row + 1):
            for col in range(min_col, max_col + 1):
                thread_img[col, row] = ThreadImager(col, row)
                thread_img[col, row].start()

        for row in range(min_row, max_row+1):
            img0 = None
            for col in range(min_col, max_col + 1):
                thread_img[col, row].join()
                image = thread_img[col, row].image
                if img0 is None:
                    img0 = image
                else:
                    img0 = np.concatenate((img0, image), axis=1)
            if img is None:
                img = img0
            else:
                img = np.concatenate((img, img0), axis=0)

        height = img.shape[0]
        width = img.shape[1]
        mask = np.zeros((height, width), dtype=np.uint8)

        p_array = []
        if parcelle.type == "Polygon":
            parcelle = [parcelle]
        for poly in list(parcelle):
            xy_array = np.array(poly.exterior.coords.xy)
            pix_x, pix_y = self.convert_to_pixel(min_col, min_row, xy_array[0], xy_array[1])
            p_array.append(np.transpose(np.array([pix_x, pix_y])))
        points = np.array(p_array)
        cv2.fillPoly(mask, points, (255))
        res = cv2.bitwise_and(img, img, mask=mask)

        points=np.concatenate(points, axis=0)
        rect = cv2.boundingRect(points)  # returns (x,y,w,h) of the rect
        cropped = res[rect[1]: rect[1] + rect[3], rect[0]: rect[0] + rect[2]]

        tl_x, tl_y = self.get_title_top_left_corner(min_col, min_row)
        return res, [tl_x, tl_y]


    def get_titles_list(self, minx, miny, maxx, maxy):
        min_col, min_row = self.convert_to_title(minx, miny)
        max_col, max_row = self.convert_to_title(maxx, maxy)
        if(min_col>max_col):
            temp = min_col
            min_col = max_col
            max_col = temp
        if(min_row>max_row):
            temp = min_row
            min_row = max_row
            max_row = temp
        return min_col, max_col, min_row, max_row


    def convert_to_pixel(self, min_col, min_row, x_list, y_list):
        X_LIST, Y_LIST = transform(Proj(init='epsg:4326'), Proj(init='epsg:3857'), x_list, y_list)
        X0 = -20037508.3427892476320267
        Y0 = 20037508.3427892476320267
        Resolution = 0.2985821417
        X1 = X0 + min_col *(256 * Resolution)
        Y1 = Y0 - min_row *(256 * Resolution)

        pixels_x = (X_LIST - X1)/Resolution
        pixels_y = (Y1 - Y_LIST)/Resolution

        pixels_x = np.around(pixels_x).astype(int)
        pixels_y = np.around(pixels_y).astype(int)
        return pixels_x, pixels_y


    def convert_from_pixel(self, top_pixel_position, x_list, y_list):
        Resolution = 0.2985821417
        x_list = top_pixel_position[0] + (x_list * Resolution)
        y_list = top_pixel_position[1] - (y_list * Resolution)
        x_list, y_list = transform(Proj(init='epsg:3857'), Proj(init='epsg:4326'), x_list, y_list)
        return x_list, y_list


    def convert_to_title(self, x, y):
        X, Y = transform(Proj(init='epsg:4326'), Proj(init='epsg:3857'), x, y)
        X0 = -20037508.3427892476320267
        Y0 = 20037508.3427892476320267
        Resolution = 0.2985821417
        TILECOL = int((X - X0) / (256 * Resolution))
        TILEROW = int((Y0 - Y) / (256 * Resolution))
        return TILECOL, TILEROW


    def get_title_top_left_corner(self, col, row):
        X0 = -20037508.3427892476320267
        Y0 = 20037508.3427892476320267
        Resolution = 0.2985821417
        X = (col * 256 * Resolution) + X0
        Y = Y0 - (row * 256 * Resolution)
        return X, Y
